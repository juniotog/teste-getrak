package main

import (
    "net/http"
	"text/template"
	"log"
	"time"
	"strings"
	"encoding/json"
	"strconv"
)

type Result struct {
    Count   int	
	Next string
	Previous string	
	Results []Starships
}
type Starships struct {
    Name	string `json:"name"`
    Consumables	string `json:"consumables"`
	MGLT	string `json:"MGLT"`
	Stops int64
}

var tmpl = template.Must(template.ParseGlob("tmpl/*"))
var httpClient = &http.Client{Timeout: 10 * time.Second}
var arrStarships []Starships

func transformConsumables(period string) int64 {

	stringSlice := strings.Split(period, " ")
	in, _ := strconv.ParseInt(stringSlice[0], 10, 64)

	if strings.Contains(period, "year") {
		return 365 * 24 * in 
	}

	if strings.Contains(period, "day") {
		return 24 * in
	}

	if strings.Contains(period, "month") {
		return 30 * 24 * in
	}

	if strings.Contains(period, "week") {
		return 7 * 24 * in
	}

	return in;
}

func Index(w http.ResponseWriter, r *http.Request) {
    tmpl.ExecuteTemplate(w, "Index", "")
}

func getAllStarships(page int) {
	result := new(Result)
	getJson("https://swapi.co/api/starships?page="+strconv.Itoa(page), result)

	for i := 0; i< len(result.Results) ;i++ {
		arrStarships = append(arrStarships, result.Results[i])
	}
	
	if len(result.Next) > 0 {
		getAllStarships(page + 1)
	}
}

func getJson(url string, target interface{}) error {
	r, err := httpClient.Get(url)
    if err != nil {
        return err
    }
    defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func Calculate(w http.ResponseWriter, r *http.Request) {
	getAllStarships(1)
	nMglt, _ := strconv.ParseInt(r.URL.Query().Get("mglt"), 10, 64) 
	
	for i := 0; i< len(arrStarships) ;i++ {
		starship := arrStarships[i]

		if starship.Consumables != "unknown" && starship.MGLT != "unknown" {
			intMGLT, _ := strconv.ParseInt(starship.MGLT, 10, 64) 
			
			hourConsumables := transformConsumables(starship.Consumables)
			hoursTravel := nMglt / intMGLT
			stops := hoursTravel / hourConsumables
			starship.Stops = stops
			arrStarships[i] = starship
		}
	}

	tmpl.ExecuteTemplate(w, "Index", arrStarships)
}

func main() {
    log.Println("Server started on: http://localhost:8080")
	http.HandleFunc("/", Index)
	http.HandleFunc("/calculate", Calculate)
    http.ListenAndServe(":8080", nil)
}