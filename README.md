# Getrak Teste

Desafio técnico para empresa Getrak.

### Instalação

Projeto desenvolvido com [GO](https://golang.org/).

Após clonar o projeto, siga os passos abaixo:

```sh
$ cd teste-getrak
$ go run main.go
```

O Sistema vai ficar disponivel em:
```sh
localhost:8080
```

### Imagem em funcionamento
[Acesse para visualizar](https://www.dropbox.com/s/aofq0uxsifri7hi/printGetrak.png?dl=0).
